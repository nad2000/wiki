package main

import (
    //"fmt"
    "io/ioutil"
    "net/http"
    "html/template"
    "regexp"
    "errors"
)

const (
    viewPath = "/view/"
    editPath = "/edit/"
    savePath = "/save/"
    lenPath = len(viewPath)
)

// Wiki page
type Page struct {
    Title string
    Body  []byte
}


func (p *Page) Render() template.HTML {
    repl := func(match []byte) []byte {
            title := match[1:len(match)-1]
            result := []byte("<a href=\"/view/")
            result = append( result, title... )
            result = append( result, []byte("\">")... )
            result = append( result, title... )
            result = append( result, []byte("</a>")... )
            return result
        }
    return template.HTML( linkRe.ReplaceAllFunc(p.Body, repl ) )
}

// Template caching:
var templates = template.Must(template.ParseFiles("tmpl/edit.html", "tmpl/view.html"))

// Page name validation:
var titleValidator = regexp.MustCompile("^[a-zA-Z0-9]+$")

// Link:
var linkRe = regexp.MustCompile(`\[[^[\]]*]`)

func (p *Page) save() error {
    filename := "data/" + p.Title + ".txt"
    return ioutil.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
    filename := "data/" + title + ".txt"
    body, err := ioutil.ReadFile(filename)
    if err != nil {
        return nil, err
    }
    return &Page{Title: title, Body: body}, nil
}


func handler(w http.ResponseWriter, r *http.Request) {
    title := r.URL.Path[1:]
    if title == "" { title = "FrontPage"}
    http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
    err := templates.ExecuteTemplate(w, tmpl+".html", p)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
    }
}

func getTitle(w http.ResponseWriter, r *http.Request) (title string, err error) {
    title = r.URL.Path[lenPath:]
    if !titleValidator.MatchString(title) {
        http.NotFound(w, r)
        err = errors.New("Invalid Page Title")
    }
    return
}

func viewHandler(w http.ResponseWriter, r *http.Request, title string) {
    p, err := loadPage(title)
    if err != nil {
        http.Redirect(w, r, "/edit/"+title, http.StatusFound)
        return
    }    
    renderTemplate( w, "view", p)
}

func editHandler(w http.ResponseWriter, r *http.Request, title string) {
    p, err := loadPage(title)
    if err != nil {
        p = &Page{Title: title}
    }
    renderTemplate( w, "edit", p)
}

func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
    body := r.FormValue("body")
    p := &Page{Title: title, Body: []byte(body)}
    err := p.save()
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
    http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
    return func(w http.ResponseWriter, r *http.Request) {
        title := r.URL.Path[lenPath:]
        if !titleValidator.MatchString(title) {
            http.NotFound(w, r)
            return
        }
        fn(w, r, title)
    }
}

func main() {
    // p1 := &Page{Title: "TestPage", Body: []byte("This is a sample Page.")}
    // p1.save()
    // p2, _ := loadPage("TestPage")
    // fmt.Println(string(p2.Body))

    http.HandleFunc(viewPath, makeHandler(viewHandler))
    http.HandleFunc(editPath, makeHandler(editHandler))
    http.HandleFunc(savePath, makeHandler(saveHandler))
    http.HandleFunc("/", handler)
    http.ListenAndServe(":8080", nil)
}